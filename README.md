# Инструкция по разворачиванию проекта

Проект состоит из следующих репозиториев:

[инфраструктурный репозиторий](https://gitlab.com/zateevas/search_engine_infra )
Здесь находится автоматизация разворачивания инфраструктуры и инфраструктурных сервисов

[rabbitmq](https://gitlab.com/zateevas/search_engine_rabbitmq)

[mongodb](https://gitlab.com/zateevas/search_engine_mongodb)

[search_engine_crawler](https://gitlab.com/zateevas/search_engine_crawler)

[search_engine_crawler](https://gitlab.com/zateevas/search_engine_ui)
 Terraform

1) создаем сервис аккаунт для gcp

IAM-Servie account-Создать- имя - роль "владелец" - создать ключ - json

export GOOGLE_CLOUD_KEYFILE_JSON=/home/zateevas/final-project-terraform.json
export GOOGLE_CREDENTIALS=/home/zateevas/final-project-terraform.json


Kubernetes

1) прописать в terraform.tfvars название проекта и путь до json файла

2) Перейти по вкладкам Compute и Kubernetes, чтобы включилось API

